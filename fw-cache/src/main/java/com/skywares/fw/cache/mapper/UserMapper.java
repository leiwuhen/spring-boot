package com.skywares.fw.cache.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.skywares.fw.cache.pojo.User;

@Mapper
public interface UserMapper
{
   User getUserById(@Param("oid")String oid);
}
