package com.skywares.fw.cache.service;

public interface CacheService<T>
{
    void put(String key ,T value);
    
    T get(String key);
}
