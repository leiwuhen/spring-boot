package com.skywares.fw.cache.config;

import java.util.concurrent.TimeUnit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

@Configuration
public class CacheConfig
{
    @Bean
    public Cache<String, Object> creatCaffeineCache() {

        return  Caffeine.newBuilder()
                //设置最后一次写入或访问后经过固定时间过期
                .expireAfterWrite(60, TimeUnit.SECONDS)
                //初始化缓存空间大小
                .initialCapacity(100)
                //最大缓存数
                .maximumSize(1000)
                .build();
    }
}
