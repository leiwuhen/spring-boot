package com.skywares.fw.cache.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.github.benmanes.caffeine.cache.Cache;
import com.skywares.fw.cache.service.CacheService;

@Service("CaffeineCacheService")
public class CaffeineCacheServiceImpl<T> implements CacheService<T>
{
    @Resource
    private Cache<String, T> caffeineCache;
    
    /**
     * 获取缓存对象
     * @param key
     * @return
     */
    public T get(String key)
    {
       return caffeineCache.asMap().get(key);
    }
    
    public void put(String key ,T value)
    {
       caffeineCache.asMap().put(key, value);
    }
}
