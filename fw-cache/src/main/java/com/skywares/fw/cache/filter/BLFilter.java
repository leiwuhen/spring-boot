package com.skywares.fw.cache.filter;

import java.nio.charset.Charset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;
import com.skywares.fw.cache.controller.UserController;

public class BLFilter
{
    private static Logger logger=LoggerFactory.getLogger(UserController.class);
    public static void main(String[] args)
    {
        // 1.创建布隆过滤器,预期数据量10000，错误率0.0001
        BloomFilter<CharSequence> bloomFilter =
                BloomFilter.create(Funnels.stringFunnel(
                        Charset.forName("utf-8")),10000, 0.0001);
        
        // 2.添加数据
        for (int i = 0; i < 5000; i++) 
        {
            bloomFilter.put("" + i);
        }
        
        logger.info("数据插入完毕");
        
        // 3.测试结果输出
        for (int i = 0; i < 10000; i++) 
        {
            if (bloomFilter.mightContain(String.valueOf(i))) 
            {
                logger.info(i + "存在");
            }
            else 
            {
                logger.info(i + "不存在");
            }
        }
    }
}
