package com.skywares.fw.cache.service.impl;

import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.skywares.fw.cache.service.CacheService;

@Service("localMapService")
public class LocalMapServiceImpl<T> implements CacheService<T>
{
    private ConcurrentHashMap<String, T> hashMap=new ConcurrentHashMap<>();
    
    @Override
    public void put(String key, T value)
    {
        hashMap.put(key, value);
    }

    @Override
    public T get(String key)
    {
        return hashMap.get(key);
    }
}
