package com.skywares.fw.cache.service;

import com.skywares.fw.cache.pojo.User;

public interface UserService
{
   User getUserById(String userId);
}
