package com.skywares.fw.cache.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.skywares.fw.cache.pojo.User;
import com.skywares.fw.cache.service.UserService;


@RestController
@RequestMapping("/user")
public class UserController
{
    private Logger logger=LoggerFactory.getLogger(UserController.class);
    
    @Autowired
    private UserService userService;
    
    @GetMapping("getUserById")
    public User getUserById(@RequestParam String userId)
    {
       logger.info("getUserByName paramter userId:"+userId);
       return userService.getUserById(userId); 
    }
}
