package com.skywares.fw.cache.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.skywares.fw.cache.mapper.UserMapper;
import com.skywares.fw.cache.pojo.User;
import com.skywares.fw.cache.service.CacheService;
import com.skywares.fw.cache.service.UserService;

@Service
@Transactional(rollbackFor=Exception.class)
public class UserServiceImpl implements UserService
{
    private Logger logger=LoggerFactory.getLogger(UserServiceImpl.class);
    
    @Qualifier("CaffeineCacheService")
    @Autowired
    private CacheService<User> cacheService;
    
    @Autowired
    private UserMapper userMapper;
    
    @Override
    public User getUserById(String userId)
    {
        User cacheUser =cacheService.get(userId);
        if(cacheUser!=null)
        {
            logger.info("get date in cache");
            return cacheUser;
        }
        
        User dbUser=userMapper.getUserById(userId);
        if(dbUser!=null)
        {
            logger.info("put data into cache");
            cacheService.put(userId, dbUser);
        }
        return dbUser;
    }

}
